# coe_control_center
Control center for the multimedia broadcasting project

## TOC
1. Installation (Ubuntu 14.04 x64)
2. Adding clients
3. Adding Groups
4. Adding Content
  a. Adding Media files
  b. Adding Documents
5. Creating a new schedule
6. Creating an Urgent Notice
7. Deployment of a schedule
8. Pushing Urgent Notice
9. Client logs
10. Troubleshooting


## 1) Installation (Ubuntu 14.04 x64)
```bash
curl https://raw.githubusercontent.com/SNUArduinoLabs/coe_control_center/master/install.sh > install.sh
chmod +x install.sh
./install_14_04.sh
source ~/.bashrc
cd webapp
export PORT=3000
export IP=0.0.0.0
whenever --update-crontab
foreman start

# Check ruby version is 2.2.0
# if not set the following
rbenv global 2.2.0
rbenv shell 2.2.0
```

In your browser, go to: `http://localhost:3000`. Default account is: `admin@arduinolabs.com` and password is `admin`







