
require 'sidekiq/web'


Rails.application.routes.draw do


  resources :marquees

  mount Sidekiq::Web => '/sidekiq'

  resources :users
  resources :contents
  resources :documents
  resources :images
  resources :groups
  resources :schedules do 
    resources :deployments, only: [:index]
    resources :content_schedules
    resources :attachments
  end

  post 'schedules/:schedule_id/attachments/:id/up_play_order' => 'attachments#up_play_order', :as => :attachment_up_play_order
  post 'schedules/:schedule_id/attachments/:id/down_play_order' => 'attachments#down_play_order', :as => :attachment_down_play_order

  post 'schedules/:schedule_id/content_schedules/:id/up_play_order' => 'content_schedules#up_play_order', :as => :content_schedule_up_play_order
  post 'schedules/:schedule_id/content_schedules/:id/down_play_order' => 'content_schedules#down_play_order', :as => :content_schedule_down_play_order

  post 'schedules/:id/deploy' => 'schedules#deploy', :as => :deploy_schedule
  post 'marquees/:id/push' => 'marquees#push', :as => :push_marquee


  get 'dashboard/clients', :as => :clients_status
  get 'dashboard/schedules', :as => :schedules_status

  get 'sessions/new'
  controller :sessions do
    get 'login' => :new
    post 'login' => :create
    delete 'logout' => :destroy
  end


  resources :users
  resources :clients


  # You can have the root of your site routed with "root"
  root 'dashboard#clients'

end
