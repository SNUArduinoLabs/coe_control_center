# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160405142912) do

  create_table "attachments", force: :cascade do |t|
    t.integer "schedule_id",                         null: false
    t.integer "document_id",                         null: false
    t.integer "slide_interval_time"
    t.integer "play_order"
    t.boolean "is_converted",        default: false
    t.integer "status",              default: 0
  end

  create_table "checksums", force: :cascade do |t|
    t.text     "filepath"
    t.text     "checksum"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clients", force: :cascade do |t|
    t.string   "ip_address"
    t.string   "college"
    t.string   "department"
    t.string   "building"
    t.string   "floor"
    t.string   "display_mode"
    t.string   "display_resolution"
    t.string   "display_model"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "storage_size"
    t.string   "storage_remained"
    t.integer  "user_id"
    t.string   "name"
  end

  add_index "clients", ["user_id"], name: "index_clients_on_user_id"

  create_table "clients_groups", id: false, force: :cascade do |t|
    t.integer "client_id", null: false
    t.integer "group_id",  null: false
  end

  create_table "content_schedules", force: :cascade do |t|
    t.integer "schedule_id",                         null: false
    t.integer "content_id",                          null: false
    t.integer "play_order"
    t.string  "slide_interval_time"
    t.string  "file_type"
    t.boolean "is_converted",        default: false
    t.integer "status",              default: 0
    t.text    "converted_file_path"
  end

  create_table "contents", force: :cascade do |t|
    t.string   "file"
    t.string   "description"
    t.string   "content_type"
    t.string   "file_path"
    t.string   "converted_file_path"
    t.string   "thumb_path"
    t.string   "duration"
    t.string   "category"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "file_tmp"
    t.string   "file_size"
    t.integer  "status",              default: 0
    t.boolean  "is_converted",        default: false
  end

  create_table "deployments", force: :cascade do |t|
    t.integer  "status",                 default: 0
    t.integer  "retries_count",          default: 0
    t.integer  "client_id"
    t.integer  "schedule_id"
    t.integer  "downloaded_files_count", default: 0
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "documents", force: :cascade do |t|
    t.string   "filename"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "groups", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "building"
    t.string   "college"
    t.string   "department"
    t.string   "group_size"
    t.string   "contact_info"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "user_id"
  end

  add_index "groups", ["user_id"], name: "index_groups_on_user_id"

  create_table "groups_schedules", id: false, force: :cascade do |t|
    t.integer "group_id"
    t.integer "schedule_id"
  end

  create_table "images", force: :cascade do |t|
    t.string   "file"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "document_id"
    t.string   "converted_file_path"
    t.boolean  "is_converted",        default: false
  end

  create_table "marquees", force: :cascade do |t|
    t.text     "content",                               null: false
    t.string   "start_hour",                            null: false
    t.string   "start_minute",                          null: false
    t.date     "start_date",                            null: false
    t.date     "end_date",                              null: false
    t.boolean  "is_shown_on_monday"
    t.boolean  "is_shown_on_tuesday"
    t.boolean  "is_shown_on_wednesday"
    t.boolean  "is_shown_on_thursday"
    t.boolean  "is_shown_on_friday"
    t.boolean  "is_shown_on_saturday"
    t.boolean  "is_shown_on_sunday"
    t.integer  "priority"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "end_hour"
    t.string   "end_minute"
    t.boolean  "is_synced",             default: false
  end

  create_table "pushes", force: :cascade do |t|
    t.integer  "client_id"
    t.integer  "marquee_id"
    t.integer  "status"
    t.integer  "retries_count"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "schedules", force: :cascade do |t|
    t.text     "description"
    t.string   "user_id"
    t.string   "total_size"
    t.string   "total_running_time"
    t.string   "status"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.text     "marquee"
    t.integer  "priority",               default: 0
    t.date     "start_date"
    t.date     "end_date"
    t.string   "start_hour"
    t.string   "start_minute"
    t.string   "end_hour"
    t.string   "end_minute"
    t.boolean  "is_played_on_monday"
    t.boolean  "is_played_on_tuesday"
    t.boolean  "is_played_on_wednesday"
    t.boolean  "is_played_on_thursday"
    t.boolean  "is_played_on_friday"
    t.boolean  "is_played_on_saturday"
    t.boolean  "is_played_on_sunday"
    t.string   "action",                 default: "add"
    t.string   "name"
    t.boolean  "delete_flag",            default: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.boolean  "is_admin"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

end
