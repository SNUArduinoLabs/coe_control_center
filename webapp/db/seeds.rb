# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.delete_all
a = User.first_or_create(:first_name => "Admin",
                         :last_name => "Admin",
                         :email => "admin@arduinolabs.com",
                         :password => "admin",
                         :password_confirmation => "admin")

a.is_admin = true
a.save()

clients = ["192.126.0.1", "192.126.0.2", "192.126.0.3", "192.126.0.4"]

Client.delete_all
clients.each do |c|
  Client.create(:ip_address => c,
                         :college => "College of Engineering",
                         :department => "CSE",
                         :building => "301",
                         :floor => "3",
                         :display_mode => "Digital (HDMI)",
                         :display_resolution => "1980x1440",
                         :display_model => "Samsung YYY",
                         :storage_size => "32GB",
                         :storage_remained => "20GB"
                        )
end




