class RenameColumnInContentSchedule < ActiveRecord::Migration
  def change
    rename_column :schedules, :priority, :play_order
  end
end
