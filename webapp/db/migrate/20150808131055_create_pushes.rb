class CreatePushes < ActiveRecord::Migration
  def change
    create_table :pushes do |t|
      t.integer :client_id
      t.integer :marquee_id
      t.integer :status
      t.integer :retries_count

      t.timestamps null: false
    end
  end
end
