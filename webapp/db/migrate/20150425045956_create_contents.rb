class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.string :filename
      t.string :description
      t.string :type
      t.string :file_path
      t.string :converted_file_path
      t.string :thumb_path
      t.string :duration
      t.boolean :is_converted
      t.string :category

      t.timestamps null: false
    end
  end
end
