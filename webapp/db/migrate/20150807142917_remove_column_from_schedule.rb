class RemoveColumnFromSchedule < ActiveRecord::Migration
  def change
    remove_column :schedules, :start_time, :time
    remove_column :schedules, :end_time, :time
  end
end
