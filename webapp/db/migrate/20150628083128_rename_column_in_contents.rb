class RenameColumnInContents < ActiveRecord::Migration
  def change
    rename_column :contents, :file_type, :content_type
  end
end
