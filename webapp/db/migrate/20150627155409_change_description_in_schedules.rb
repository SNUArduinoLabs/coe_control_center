class ChangeDescriptionInSchedules < ActiveRecord::Migration
  def change
    change_column :schedules, :description, :text
  end
end
