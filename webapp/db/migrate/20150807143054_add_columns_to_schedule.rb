class AddColumnsToSchedule < ActiveRecord::Migration
  def change
    add_column :schedules, :start_hour, :string
    add_column :schedules, :start_minute, :string
    add_column :schedules, :end_hour, :string
    add_column :schedules, :end_minute, :string
  end
end
