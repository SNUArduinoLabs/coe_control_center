class RemoveColumnFromContent < ActiveRecord::Migration
  def change
    remove_column :contents, :is_converted, :string
  end
end
