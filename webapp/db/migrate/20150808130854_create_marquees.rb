class CreateMarquees < ActiveRecord::Migration
  def change
    create_table :marquees do |t|
      t.text :content, null: false
      t.string :start_hour, null: false
      t.string :start_minute, null: false
      t.date :start_date, null: false
      t.date :end_date, null: false
      t.boolean :is_shown_on_monday
      t.boolean :is_shown_on_tuesday
      t.boolean :is_shown_on_wednesday
      t.boolean :is_shown_on_thurday
      t.boolean :is_shown_on_friday
      t.boolean :is_shown_on_saturday
      t.boolean :is_shown_on_sunday
      t.integer :priority

      t.timestamps null: false
    end
  end
end
