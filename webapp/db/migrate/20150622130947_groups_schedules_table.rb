class GroupsSchedulesTable < ActiveRecord::Migration
  def change
    create_table :groups_schedules, :id => false do |t|
      t.integer :group_id
      t.integer :schedule_id
    end
  end
end
