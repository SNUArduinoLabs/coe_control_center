class CreateJoinTableSchedulesClients < ActiveRecord::Migration
  def change
    create_join_table :schedules, :clients do |t|
      # t.index [:schedule_id, :client_id]
      # t.index [:client_id, :schedule_id]
    end
  end
end
