class AddColumnsToContentsSchedules < ActiveRecord::Migration
  def change
    add_column :contents_schedules, :priority, :integer
    add_column :contents_schedules, :slide_interval_time, :string
  end
end
