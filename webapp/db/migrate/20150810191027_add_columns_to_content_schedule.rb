class AddColumnsToContentSchedule < ActiveRecord::Migration
  def change
    add_column :content_schedules, :is_converted, :boolean, default: false
    add_column :content_schedules, :status, :integer, default: 0
  end
end
