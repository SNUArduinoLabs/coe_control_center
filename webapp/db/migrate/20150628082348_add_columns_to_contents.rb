class AddColumnsToContents < ActiveRecord::Migration
  def change
    add_column :contents, :file_size, :string
    rename_column :contents, :type, :file_type
  end
end
