class AddColumn3ToContent < ActiveRecord::Migration
  def change
    add_column :contents, :is_converted, :boolean, default: false
  end
end
