class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name
      t.string :description
      t.string :building
      t.string :college
      t.string :department
      t.string :group_size
      t.string :contact_info

      t.timestamps null: false
    end
  end
end
