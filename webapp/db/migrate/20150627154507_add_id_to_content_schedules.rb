class AddIdToContentSchedules < ActiveRecord::Migration
  def change
    add_column :content_schedules, :id, :primary_key
  end
end
