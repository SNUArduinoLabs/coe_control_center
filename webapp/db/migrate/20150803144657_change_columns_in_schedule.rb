class ChangeColumnsInSchedule < ActiveRecord::Migration
  def change
    change_column :schedules, :start_time, :time
    change_column :schedules, :end_time, :time
    add_column :schedules, :start_day, :date
    add_column :schedules, :end_day, :date
  end
end
