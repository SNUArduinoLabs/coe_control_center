class CreateTable < ActiveRecord::Migration
  def change
    create_table :documents_schedules, force: :cascade do |t|
      t.integer "schedule_id", null: false
      t.integer "document_id", null: false
      
    end
  end
end
