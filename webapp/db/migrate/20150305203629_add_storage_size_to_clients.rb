class AddStorageSizeToClients < ActiveRecord::Migration
  def change
    add_column :clients, :storage_size, :string
  end
end
