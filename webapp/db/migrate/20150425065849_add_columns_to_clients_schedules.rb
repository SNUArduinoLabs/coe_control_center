class AddColumnsToClientsSchedules < ActiveRecord::Migration
  def change
    add_column :clients_schedules, :deployed_at, :string
    add_column :clients_schedules, :is_downloaded, :boolean
  end
end
