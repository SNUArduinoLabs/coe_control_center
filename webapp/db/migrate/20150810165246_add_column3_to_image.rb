class AddColumn3ToImage < ActiveRecord::Migration
  def change
    add_column :images, :is_converted, :boolean, default: false
  end
end
