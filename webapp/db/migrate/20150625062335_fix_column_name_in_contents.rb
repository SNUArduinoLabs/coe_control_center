class FixColumnNameInContents < ActiveRecord::Migration
  def change
    rename_column :contents, :filename, :file
  end
end
