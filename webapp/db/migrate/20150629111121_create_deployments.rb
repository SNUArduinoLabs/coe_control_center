class CreateDeployments < ActiveRecord::Migration
  def change
    create_table :deployments do |t|
      t.integer :status
      t.integer :retries_count
      t.integer :client_id
      t.integer :schedule_id
      t.integer :downloaded_files_count

      t.timestamps null: false
    end
  end
end
