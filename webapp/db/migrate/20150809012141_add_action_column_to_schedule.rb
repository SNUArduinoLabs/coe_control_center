class AddActionColumnToSchedule < ActiveRecord::Migration
  def change
    add_column :schedules, :action, :string, default: "add"
  end
end
