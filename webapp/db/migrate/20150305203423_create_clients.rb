class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :ip_address
      t.string :college
      t.string :department
      t.string :building
      t.string :floor
      t.string :display_mode
      t.string :display_resolution
      t.string :display_model

      t.timestamps null: false
    end
  end
end
