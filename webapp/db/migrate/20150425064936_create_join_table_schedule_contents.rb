class CreateJoinTableScheduleContents < ActiveRecord::Migration
  def change
    create_join_table :schedules, :contents do |t|
      # t.index [:schedule_id, :content_id]
      # t.index [:content_id, :schedule_id]
    end
  end
end
