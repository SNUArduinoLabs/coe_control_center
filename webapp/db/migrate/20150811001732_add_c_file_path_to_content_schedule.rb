class AddCFilePathToContentSchedule < ActiveRecord::Migration
  def change
    add_column :content_schedules, :converted_file_path, :text
  end
end
