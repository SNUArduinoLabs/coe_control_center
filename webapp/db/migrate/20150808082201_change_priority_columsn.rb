class ChangePriorityColumsn < ActiveRecord::Migration
  def change
    rename_column :schedules, :play_order, :priority
    rename_column :content_schedules, :priority, :play_order
  end
end
