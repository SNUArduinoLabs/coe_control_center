class AddDaysOfWeeToSchedule < ActiveRecord::Migration
  def change
    add_column :schedules, :is_played_on_monday, :integer
    add_column :schedules, :is_played_on_tuesday, :integer
    add_column :schedules, :is_played_on_wednesday, :integer
    add_column :schedules, :is_played_on_thursday, :integer
    add_column :schedules, :is_played_on_friday, :integer
    add_column :schedules, :is_played_on_saturday, :integer
    add_column :schedules, :is_played_on_sunday, :integer
  end
end
