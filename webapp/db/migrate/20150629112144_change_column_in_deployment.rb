class ChangeColumnInDeployment < ActiveRecord::Migration
  def change
    change_column :deployments, :status, :integer, default: 0
    change_column :deployments, :retries_count, :integer, default: 0
  end
end
