class CreateChecksums < ActiveRecord::Migration
  def change
    create_table :checksums do |t|
      t.text :filepath
      t.text :checksum

      t.timestamps null: false
    end
  end
end
