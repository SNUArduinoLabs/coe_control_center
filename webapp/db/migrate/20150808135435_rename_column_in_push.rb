class RenameColumnInPush < ActiveRecord::Migration
  def change
    rename_column :marquees, :is_shown_on_thurday, :is_shown_on_thursday
  end
end
