class AddColumnToMarquee < ActiveRecord::Migration
  def change
    add_column :marquees, :end_hour, :string
    add_column :marquees, :end_minute, :string
  end
end
