class CreateJoinTable < ActiveRecord::Migration
  def change
    create_join_table :clients, :groups do |t|
      # t.index [:client_id, :group_id]
      # t.index [:group_id, :client_id]
    end
  end
end
