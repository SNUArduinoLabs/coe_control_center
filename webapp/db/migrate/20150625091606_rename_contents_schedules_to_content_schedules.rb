class RenameContentsSchedulesToContentSchedules < ActiveRecord::Migration
  def change
    rename_table :contents_schedules, :content_schedules
  end
end
