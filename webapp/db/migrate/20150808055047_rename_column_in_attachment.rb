class RenameColumnInAttachment < ActiveRecord::Migration
  def change
    rename_column :attachments, :priority, :play_order
  end
end
