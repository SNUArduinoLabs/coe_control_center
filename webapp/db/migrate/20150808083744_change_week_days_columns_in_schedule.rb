class ChangeWeekDaysColumnsInSchedule < ActiveRecord::Migration
  def change
    change_column :schedules, :is_played_on_monday, :boolean
    change_column :schedules, :is_played_on_tuesday, :boolean
    change_column :schedules, :is_played_on_wednesday, :boolean
    change_column :schedules, :is_played_on_thursday, :boolean
    change_column :schedules, :is_played_on_friday, :boolean
    change_column :schedules, :is_played_on_saturday, :boolean
    change_column :schedules, :is_played_on_sunday, :boolean

  end
end
