class ChangeColumnsInDeployment < ActiveRecord::Migration
  def change
    change_column :deployments, :downloaded_files_count, :integer, :default => 0
  end
end
