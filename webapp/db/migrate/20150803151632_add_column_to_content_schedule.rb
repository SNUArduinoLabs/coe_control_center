class AddColumnToContentSchedule < ActiveRecord::Migration
  def change
    add_column :content_schedules, :file_type, :string
  end
end
