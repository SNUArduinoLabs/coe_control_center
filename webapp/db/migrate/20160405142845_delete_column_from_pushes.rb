class DeleteColumnFromPushes < ActiveRecord::Migration
  def change
    remove_column :pushes, :is_synced
  end
end
