class CreatePdfContents < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :filename
      t.string :description
      t.integer :slide_interval

      t.timestamps null: false
    end
  end
end
