class AddStatusToContent < ActiveRecord::Migration
  def change
    add_column :contents, :status, :integer, default: 0
  end
end
