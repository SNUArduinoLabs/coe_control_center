class AddColumnToMarquees < ActiveRecord::Migration
  def change
    add_column :marquees, :is_synced, :boolean, default: false
  end
end
