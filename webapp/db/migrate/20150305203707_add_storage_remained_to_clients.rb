class AddStorageRemainedToClients < ActiveRecord::Migration
  def change
    add_column :clients, :storage_remained, :string
  end
end
