class RenameSlideIntervalInAttachment < ActiveRecord::Migration
  def change
    rename_column :attachments, :slide_interval, :slide_interval_time
  end
end
