class AddIsDestroyedColumnToSchedule < ActiveRecord::Migration
  def change
    add_column :schedules, :delete_flag, :boolean, default: false
  end
end
