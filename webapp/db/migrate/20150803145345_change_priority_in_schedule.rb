class ChangePriorityInSchedule < ActiveRecord::Migration
  def change
    change_column :schedules, :priority, :integer, default: 0
  end
end
