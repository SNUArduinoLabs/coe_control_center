class AddColumnToContents < ActiveRecord::Migration
  def change
    add_column :contents, :file_tmp, :string
  end
end
