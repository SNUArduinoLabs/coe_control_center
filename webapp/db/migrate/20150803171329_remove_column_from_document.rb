class RemoveColumnFromDocument < ActiveRecord::Migration
  def change
    remove_column :documents, :slide_interval, :string
  end
end
