class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.string :description
      t.string :start_time
      t.string :end_time
      t.string :user_id
      t.string :total_size
      t.string :total_running_time
      t.string :status

      t.timestamps null: false
    end
  end
end
