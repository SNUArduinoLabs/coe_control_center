class AddColumnToDocumentsSchedule < ActiveRecord::Migration
  def change
    add_column :documents_schedules, :slide_interval, :integer
  end
end
