class AddColumn2ToImage < ActiveRecord::Migration
  def change
    add_column :images, :converted_file_path, :string
  end
end
