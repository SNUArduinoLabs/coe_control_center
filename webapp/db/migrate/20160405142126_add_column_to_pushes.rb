class AddColumnToPushes < ActiveRecord::Migration
  def change
    add_column :pushes, :is_synced, :boolean, default: false
  end
end
