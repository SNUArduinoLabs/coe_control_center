class AddColumnsToAttachments < ActiveRecord::Migration
  def change
    add_column :attachments, :is_converted, :boolean, default: false
    add_column :attachments, :status, :integer, default: 0
  end
end
