class RemoveClientScheduleTable < ActiveRecord::Migration
  def change
    drop_table :clients_schedules
  end
end
