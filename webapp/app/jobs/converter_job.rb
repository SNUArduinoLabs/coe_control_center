class ConverterJob < ActiveJob::Base
  queue_as :conversion


  def perform(model, dst_path, opts = {}) 

    filename = dst_path.split("/")[-1].split(".")[0]

    # ffmpeg -loop 1 -i image.jpg -c:v libx264 -t 30 -pix_fmt yuv420p output.mp4

    new_path = dst_path.split(".")[0].gsub("video_", "") + ".mp4"
    new_path = Rails.root.join('public', new_path)

    dst_path = dst_path.gsub("video_", "")
    dst_path = Rails.root.join('public', dst_path)

    puts "dst_path is " + dst_path.to_s

    unless (File.exists?(dst_path))
      raise
    end

    str = "ffmpeg -loop 1 -y" +
          " -i " + dst_path.to_s +
          " -vf scale=1280:-2" +
          " -c:v libx264 -t 5" +
          " -pix_fmt yuv420p " + new_path.to_s

    puts "Executing command: "
    puts str 

    system(str)

    model.set_converted_filepath!(new_path.to_s)

  end

  # def perform(tmpfile, encoded_file, model, dst_path, opts = {})
  #   file = ::FFMPEG::Movie.new(tmpfile)
  #   if file.valid? # true (would be false if ffmpeg fails to read the movie)
  #     #file.screenshot("screenshot.png", { seek_time: 10, resolution: '400x300' })
  #     #file.transcode(encoded_file, opts) { |progress| puts "#{(progress * 100).round(2)} %" }
  #     file.transcode(encoded_file, "-s hd720 -vcodec libx264 -strict -2") { |progress| puts "#{(progress * 100).round(2)} %" }
  #     #file.transcode(encoded_file, opts)


  #     dst_path = File.join(Rails.public_path, dst_path)
  #     File.rename(encoded_file, dst_path)
  #     model.set_converted_file_path!(dst_path)
  #     model.finished!
  #   else
  #     model.error!
  #   end
  #   #File.delete(tmpfile)
  # end


end
