
require 'net/http'
require 'json'

class ClientStatusJob < ActiveJob::Base
  queue_as :cron_jobs

  def perform(client_id)
    client = Client.find_by_id(client_id)

    if not client.nil?
      url = "http://#{client[:ip_address]}:#{$CLIENT_WEBAPP_PORT}/status?numOfLines=100,100,100"
      uri = URI.parse(url)

      p "Geting status from client: #{url}"

      http = Net::HTTP.new(uri.host, uri.port)

      request = Net::HTTP::Get.new(uri.request_uri)
      request["User-Agent"] = "COE_CONTROL_CENTER"
      request["Accept"] = "*/*"

      begin
        response = http.request(request)

        if (response.code.to_i == 200)
          json_object = JSON.parse(response.body)

          $redis.set($REDIS_CLIENT_PREFIX + "#{client[:ip_address]}", json_object.to_json)
          $redis.set($REDIS_WEBAPP_LOG + "#{client[:ip_address]}", json_object["logs"]["webapp"].to_json)
          $redis.set($REDIS_DOWNLOADER_LOG + "#{client[:ip_address]}", json_object["logs"]["downloader"].to_json)
          $redis.set($REDIS_PLAYER_LOG + "#{client[:ip_address]}", json_object["logs"]["player"].to_json)
          $redis.set($REDIS_OMXD_LOG + "#{client[:ip_address]}", json_object["logs"]["omxd"].to_json)
          $redis.set($REDIS_CURRENTLY_PLAYING_PREFIX + "#{client[:ip_address]}", json_object["playing"].to_json)
          $redis.set($REDIS_CURRENTLY_DOWNLOADING_PREFIX + "#{client[:ip_address]}", json_object["downloading"].to_json)

          p "Saved the response successfully to redis"

        else
          p "Invalid response from client: #{client[:ip_address]}. Response code is not 200"
        end

      rescue Timeout::Error, Errno::ETIMEDOUT, Errno::EINVAL, Errno::ECONNRESET, EOFError,
         Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError, Net::ProtocolError => e
         p "Failed to get any response from the client #{client[:ip_address]}"
      end
    end
    
  end
end
