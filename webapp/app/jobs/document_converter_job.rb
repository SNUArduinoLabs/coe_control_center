
class DocumentConverterJob < ActiveJob::Base
  queue_as :conversion

  def perform(model, interval, opts = {}) 
    # TODO

    puts "DocumentConverterJob"
    puts "Interval is " + interval.to_s

    images = model.document.images

    images.each do |img|
      
      img_filepath = img.get_file_path
      new_filepath = img.get_converted_file_path(interval)

      img_filepath = Rails.root.join('public', img_filepath)
      new_filepath = Rails.root.join('public', new_filepath)

      puts "Converting img: " + img.id.to_s
      puts "file_path is " + img_filepath.to_s
      puts "new_file_path is " + new_filepath.to_s

      if (File.exists?(new_filepath))
        puts "File already exists"
        next
      end

      str = "ffmpeg -loop 1 -y" +
            " -i " + img_filepath.to_s +
            " -c:v libx264 -t " + interval +
            " -pix_fmt yuv420p " + new_filepath.to_s

      puts "Executing command: "
      puts str 

      system(str)

      Checksum.add_new_file!(new_filepath.to_s)
      
      puts "Finished converting img: " + img.id.to_s

    end

    model.set_converted!(true)
    puts "Document conversion done"

  end
end