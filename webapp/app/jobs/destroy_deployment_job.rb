

require 'net/http'


class DestroyDeploymentJob < ActiveJob::Base
  queue_as :default

  def perform(deployment_id)

    deployment = Deployment.find_by_id(deployment_id)
    client = deployment.client
    schedule = deployment.schedule

    url = "http://#{client[:ip_address]}:#{$CLIENT_WEBAPP_PORT}/schedule"

    p "Sending to client: #{url}"

    payload = schedule.get_json_object("DEL")

    p payload

    uri = URI.parse(url)

    req = Net::HTTP::Post.new(uri, initheader = {'Content-Type' =>'application/json'})
    req.body = payload

    deployment.delete_started!
    deployment.increment!(:retries_count)

    begin   
      response = Net::HTTP.start(uri.hostname, uri.port) do |http|
        http.request(req)
      end
      puts "Response #{response.code} #{response.message}:#{response.body}"

      if not (response.code.to_i >= 200 && response.code.to_i < 300)
        deployment.delete_error!
      else 
        deployment.delete_finished!
      end

    rescue Timeout::Error, Errno::ETIMEDOUT, Errno::EINVAL, Errno::ECONNRESET, EOFError,
       Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError, Net::ProtocolError => e
      deployment.delete_error!
    end 

  end
end


