class DestroyPushJob < ActiveJob::Base

  queue_as :default

  def perform(push_id)

    push = Push.find_by_id(push_id)
    client = push.client
    marquee = push.marquee

    url = "http://#{client[:ip_address]}:#{$CLIENT_WEBAPP_PORT}/marquee"

    p "Sending Maquee text to client: #{url}"
    p "Payload is : #{marquee.get_json_object}"

    payload = marquee.get_json_object("DEL")
    uri = URI.parse(url)

    req = Net::HTTP::Post.new(uri, initheader = {'Content-Type' =>'application/json'})
    req.body = payload

    push.delete_started!
    push.increment!(:retries_count)

    begin   
      response = Net::HTTP.start(uri.hostname, uri.port) do |http|
        http.request(req)
      end
      puts "Response #{response.code} #{response.message}:#{response.body}"

      if not (response.code.to_i >= 200 && response.code.to_i < 300)
        push.delete_error!
      else
        push.delete_finished!
      end

    rescue Timeout::Error, Errno::ETIMEDOUT, Errno::EINVAL, Errno::ECONNRESET, EOFError,
       Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError, Net::ProtocolError => e
      push.delete_error!
    end 

  end






end
