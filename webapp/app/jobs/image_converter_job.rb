class ImageConverterJob < ActiveJob::Base
  queue_as :conversion


  def perform(model, orig_file_path, new_file_path, interval, opts = {}) 

    # ffmpeg -loop 1 -i image.jpg -c:v libx264 -t 30 -pix_fmt yuv420p output.mp4

    puts "ImageConverterJob"
    puts "file_path is " + orig_file_path.to_s
    puts "new_file path is " + new_file_path.to_s

    if (File.exists?(new_file_path))
      model.set_converted_filepath!(new_file_path.to_s)
      puts "File already exists"
      return
    end

    str = "ffmpeg -loop 1 -y" +
          " -i " + orig_file_path.to_s +
          " -vf scale=1280:-2" +
          " -c:v libx264 -t " + interval +
          " -pix_fmt yuv420p " + new_file_path.to_s

    puts "Executing command: "
    puts str 

    system(str)

    Checksum.add_new_file!(new_file_path.to_s)

    model.set_converted_filepath!(new_file_path.to_s)
    puts "File conversion done"



  end

end
