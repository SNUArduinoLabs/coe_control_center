
require 'net/http'


class DeploymentJob < ActiveJob::Base
  queue_as :default

  def perform(deployment_id)

    deployment = Deployment.find_by_id(deployment_id)
    client = deployment.client
    schedule = deployment.schedule

    url = "http://#{client[:ip_address]}:#{$CLIENT_WEBAPP_PORT}/schedule"

    p "Sending to client: #{url}"
    p "Payload is : #{schedule.get_json_object}"

    payload = schedule.get_json_object
    uri = URI.parse(url)

    req = Net::HTTP::Post.new(uri, initheader = {'Content-Type' =>'application/json'})
    req.body = payload

    deployment.started!
    deployment.increment!(:retries_count)

    begin   
      response = Net::HTTP.start(uri.hostname, uri.port) do |http|
        http.request(req)
      end
      puts "Response #{response.code} #{response.message}:#{response.body}"

      if not (response.code.to_i >= 200 && response.code.to_i < 300)
        deployment.error!
      else 
        deployment.finished!
      end

    rescue Timeout::Error, Errno::ETIMEDOUT, Errno::EINVAL, Errno::ECONNRESET, EOFError,
       Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError, Net::ProtocolError => e
      deployment.error!
    end 

  end
end
