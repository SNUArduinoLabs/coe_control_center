class Image < ActiveRecord::Base

  belongs_to :document
  validates :file, presence: true
  mount_uploader :file, ImageUploader


  UPLOAD_PATH = Rails.root.join('public', 'uploads/image/file/')

  def get_converted_file_path(interval) 
    UPLOAD_PATH.join(self.id.to_s, self.id.to_s + "_" + interval.to_s + ".mp4").to_s
  end

  def get_checksum_of(interval)
    filepath = self.get_converted_file_path(interval)
    Checksum.get_checksum_of!(filepath)
  end

  def get_file_path
    self.file.file.file.to_s
  end

end
