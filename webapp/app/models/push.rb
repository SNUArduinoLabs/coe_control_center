class Push < ActiveRecord::Base

  belongs_to :marquee
  belongs_to :client

  enum status: [ :not_started, :started, :finished, :error, :delete_started, :delete_finished, :delete_error ]

  def push!
    update_attributes(:retries_count => 0)
    PushJob.perform_later(self.id) if not finished!
  end


end
