class Attachment < ActiveRecord::Base
  belongs_to :schedule
  belongs_to :document

  validates_presence_of :play_order
  validates_presence_of :slide_interval_time

  validates_numericality_of :play_order
  validates_numericality_of :slide_interval_time, :greater_than_or_equal_to => 4

  validates :schedule, :presence => true
  validates :document, :presence => true

  def set_converted!(value)
    self.update_attributes(:is_converted => value)
    self.save!
  end


end
