class Document < ActiveRecord::Base

  has_many :images

  has_many :attachments
  has_many :schedules, :through => :attachments

  accepts_nested_attributes_for :images, allow_destroy: true, reject_if: proc { |attr| attr.file.blank? }

  validates :images, :presence => true

  def is_converted
    flag = true
    self.images.each do |img|
      if img.converted_file_path.blank?
        flag = false
      end
    end


    flag
  end


end
