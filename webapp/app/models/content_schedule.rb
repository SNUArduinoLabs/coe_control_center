class ContentSchedule < ActiveRecord::Base
	belongs_to :content 
	belongs_to :schedule

  accepts_nested_attributes_for :content

  validates_presence_of :play_order
  validates_presence_of :slide_interval_time

  validates_numericality_of :play_order
  validates_numericality_of :slide_interval_time, :greater_than_or_equal_to => 4

  validates :content, :presence => true
  validates :schedule, :presence => true

  UPLOAD_PATH = Rails.root.join('public', 'uploads/content/file/')

  def get_converted_file_path(interval) 
    con = self.content
    UPLOAD_PATH.join(con.id.to_s, con.id.to_s + "_" + interval.to_s + ".mp4").to_s
  end

  def get_checksum
    filepath = self.get_converted_file_path(self.slide_interval_time)
    Checksum.get_checksum_of!(filepath)
  end

  def set_converted_filepath!(new_path)
    self.update_attributes(:converted_file_path => new_path)
    self.update_attributes(:is_converted => true)
    self.save!
  end

end

