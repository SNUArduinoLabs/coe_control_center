class Marquee < ActiveRecord::Base

  after_update :reinitialize_pushes

  has_many :pushes, :dependent => :delete_all
  has_many :clients, :through => :pushes

  enum :priority => [:low, :medium, :high]

  HOURS = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11",
           "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"]

  MINUTES = [ "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11",
              "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23",
              "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", 
              "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", 
              "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"]


  DAYS_OF_WEEK_COLUMNS = ['is_shown_on_monday', 
                          'is_shown_on_tuesday', 
                          'is_shown_on_wednesday', 
                          'is_shown_on_thursday', 
                          'is_shown_on_friday', 
                          'is_shown_on_saturday', 
                          'is_shown_on_sunday']

  DAYS_OF_WEEK = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']

  # Validations
  validates_presence_of :start_hour
  validates_presence_of :start_minute
  validates_presence_of :end_hour
  validates_presence_of :end_minute
  validates_presence_of :start_date
  validates_presence_of :end_date
  validates_presence_of :content

  validates_inclusion_of :start_hour, :in => Marquee::HOURS
  validates_inclusion_of :start_minute, :in => Marquee::MINUTES
  validates_inclusion_of :end_hour, :in => Marquee::HOURS
  validates_inclusion_of :end_minute, :in => Marquee::MINUTES

  def get_start_time
    self.start_hour + ":" + self.start_minute + ":00"
  end

  def get_end_time
    self.end_hour + ":" + self.end_minute + ":00"
  end

  def get_start_date_time
    self.start_date.to_s + " " + self.get_start_time
  end

  def get_end_date_time
    self.end_date.to_s + " " + self.get_end_time
  end

  def get_days_of_the_week
    # self is not a class/module (cannot send)
    flags = DAYS_OF_WEEK_COLUMNS.collect { |day| self.method(day.to_sym).call() }
    results = []
    flags.each_with_index do |val, index|
      if val
        results << DAYS_OF_WEEK[index]
      end
    end
    results
  end

  def get_json_object(action = "add")
    return {
      "text" => self.content,
      "action" => action,
      "start_datetime" => self.get_start_date_time,
      "end_datetime" => self.get_end_date_time,
      "days_of_week" => self.get_days_of_the_week
      }.to_json
  end

  def push!
    self.pushes.each do |push|
      PushJob.perform_later(push[:id])
    end
  end


private
    def reinitialize_pushes
    self.pushes.each do |push|
      push.not_started!
    end
  end


end
