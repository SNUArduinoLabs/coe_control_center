class Schedule < ActiveRecord::Base

  after_update :reinitialize_deployments
  after_update :update_action_column

  has_many :content_schedules, :dependent => :delete_all
  has_many :contents, :through => :content_schedules

  has_many :attachments, :dependent => :delete_all
  has_many :documents, :through => :attachments

  #has_and_belongs_to_many :clients
  has_many :deployments, :dependent => :delete_all
  has_many :clients, :through => :deployments

  # TODO: have to take care of groups
  #has_and_belongs_to_many :groups

  accepts_nested_attributes_for :content_schedules, allow_destroy: true, :reject_if => lambda { |c| c[:play_order].blank? }

  enum :priority => [:low, :medium, :high]

  HOURS = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11",
           "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"]

  MINUTES = [ "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11",
              "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23",
              "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", 
              "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", 
              "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"]


  DAYS_OF_WEEK_COLUMNS = ['is_played_on_monday', 
                          'is_played_on_tuesday', 
                          'is_played_on_wednesday', 
                          'is_played_on_thursday', 
                          'is_played_on_friday', 
                          'is_played_on_saturday', 
                          'is_played_on_sunday']

  DAYS_OF_WEEK = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']

  # Validations
  validates_presence_of :start_hour
  validates_presence_of :start_minute
  validates_presence_of :end_hour
  validates_presence_of :end_minute
  validates_presence_of :start_date
  validates_presence_of :end_date
  validates_presence_of :priority
  validates_presence_of :description
  validates_presence_of :name

  validates_inclusion_of :start_hour, :in => Schedule::HOURS
  validates_inclusion_of :start_minute, :in => Schedule::MINUTES
  validates_inclusion_of :end_hour, :in => Schedule::HOURS
  validates_inclusion_of :end_minute, :in => Schedule::MINUTES

  # validates_inclusion_of :is_played_on_monday, :in => [0,1]
  # validates_inclusion_of :is_played_on_tuesday, :in => [0,1]
  # validates_inclusion_of :is_played_on_wednesday, :in => [0,1]
  # validates_inclusion_of :is_played_on_thursday, :in => [0,1]
  # validates_inclusion_of :is_played_on_friday, :in => [0,1]
  # validates_inclusion_of :is_played_on_saturday, :in => [0,1]
  # validates_inclusion_of :is_played_on_sunday, :in => [0,1]


  def get_start_time
    self.start_hour + ":" + self.start_minute + ":00"
  end

  def get_end_time
    self.end_hour + ":" + self.end_minute + ":00"
  end

  def get_start_date_time
    self.start_date.to_s + " " + self.get_start_time
  end

  def get_end_date_time
    self.end_date.to_s + " " + self.get_end_time
  end

  def get_days_of_the_week
    # self is not a class/module (cannot send)
    flags = DAYS_OF_WEEK_COLUMNS.collect { |day| self.method(day.to_sym).call() }
    results = []
    flags.each_with_index do |val, index|
      if val
        results << DAYS_OF_WEEK[index]
      end
    end
    results
  end

  def get_all_contents_and_attachments
    everything = self.attachments + self.content_schedules
    everything.sort_by(&:play_order)
  end

  def get_max_play_order
    all_files = self.get_all_contents_and_attachments
    if all_files.any?
      all_files.map(&:play_order).max
    else
      0
    end
  end

  def rearrange_play_order!
    atts = self.attachments.sort_by(&:play_order)
    cons = self.content_schedules.sort_by(&:play_order)

    if atts.empty? and cons.empty?
      return
    end

    all_files = []

    if atts.any? and atts[0].play_order == 1
      all_files = atts + cons
    else
      all_files = cons + atts
    end

    all_files.each_with_index do |obj, index|
      obj.update_attributes(:play_order => index+1)
      obj.save!
    end

  end

  # Give the union of this schedule's clients and this schedule's groups' clients
  def all_clients
    self.clients
  	#clients | groups.map(&:clients).flatten
  end

  def is_deployable
    flag = true
    self.attachments.each do |att|
      if not att.is_converted
        flag = false
      end
    end

    self.content_schedules.each do |cs|
      if not cs.is_converted
        flag = false
      end
    end
    flag and (not self.delete_flag)
  end

  def deploy!
    flag = self.is_deployable()

    if flag
      self.deployments.each do |deployment|
        deployment.update_attributes(:retries_count => 0)
        DeploymentJob.perform_later(deployment[:id])
      end
    end

    return flag
  end

  def get_json_object(action = "add")
    prioritized = {}
    min = 1
    max = 1

    content_files = []
    self.content_schedules.each do |cs|
      content = cs.content
      prioritized[cs[:play_order]] = {
        :type => :content,
        :obj => cs
      }
    end

    document_files = []
    self.attachments.each do |a|
      doc = a.document
      prioritized[a[:play_order]] = {
        :type => :document,
        :obj => a
      }
    end

    all_contents = []

    current_play_order = 1
    prioritized.keys.sort.each do |i| 
      elm = prioritized[i]

      if elm[:type] == :document
        attachment = elm[:obj]
        doc = attachment.document
        images = doc.images.sort_by { |x| x.get_converted_file_path(attachment.slide_interval_time) }
        images.each do |i|
          old_path = i.get_converted_file_path(attachment.slide_interval_time)
          new_path = Checksum.get_checksum_file_path!(old_path)
          all_contents << {
            "filename" => new_path.split('/').last,
            "type" => "VIDEO",                   # PDF are always JPG
            "path" => new_path,
            "play_order" => current_play_order,
            "interval" => attachment["slide_interval_time"]
          }

          current_play_order += 1
        end
      else
        # Content file (image/video)
        cs = elm[:obj]
        content = cs.content

        if type_map(content.content_type) == "VIDEO"
          old_path = Rails.root.join('public', content.file.file.file).to_s
          new_path = Checksum.get_checksum_file_path!(old_path)
          all_contents << {
            "filename" => new_path.split("/").last,
            "type" => type_map(content.content_type),
            "path" => new_path,
            "play_order" => cs[:play_order],
            "interval" => cs[:slide_interval_time]
          }
        else
          old_path = cs.get_converted_file_path(cs.slide_interval_time)
          new_path = Checksum.get_checksum_file_path!(old_path)
          all_contents << {
            "filename" => new_path.split("/").last,
            "type" => "VIDEO",               # converting IMAGES to VIDEO
            "path" => new_path,
            "play_order" => cs[:play_order],
            "interval" => cs[:slide_interval_time]
          }
        end

        current_play_order += 1

      end
    end

    return {
      "schedule_id" => self.id,
      "schedule_name" => self.name ? self.name : "",
      "schedule_desc" => self.description,
      "priority" => Schedule.priorities[self.priority],
      "action" => action,
      "start_datetime" => self.get_start_date_time,
      "end_datetime" => self.get_end_date_time,
      "days_of_week" => self.get_days_of_the_week,
      "marquee_text" => self.marquee,
      "content_files" => all_contents
    }.to_json
  end



  protected

  def reinitialize_deployments
    self.deployments.each do |deployment|
      deployment.not_started!
    end
  end

  def update_action_column
    self.action = "update"
  end

  def type_map(content_type)
    if content_type
      if content_type.starts_with?("video")
        "VIDEO"
      elsif content_type.starts_with?("image")
        "IMAGE"
      elsif content_type.starts_with?("application/pdf")
        "PDF"
      end
    else
      "UKNOWN"
    end
  end

end
