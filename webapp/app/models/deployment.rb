class Deployment < ActiveRecord::Base

  enum status: [ :not_started, :started, :finished, :error, :delete_started, :delete_finished, :delete_error ]

  belongs_to :schedule
  belongs_to :client


  def all_contents
    self.schedule.contents
  end

  def deploy!
    DeploymentJob.perform_later(self.id) if not finished!
  end

end
