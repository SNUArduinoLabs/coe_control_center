
require 'digest/sha1'

class Checksum < ActiveRecord::Base

  def self.get_checksum_of!(filepath)
    row = Checksum.find_by(:filepath => filepath)
    if row.nil?
      puts "New checksum created"
      checksum = Digest::SHA1.hexdigest(File.read(filepath))
      new_row = Checksum.new(:filepath => filepath, :checksum => checksum)
      new_row.save
      new_row.checksum
    else
      row.checksum
    end
  end

  def self.add_new_file!(filepath)
    checksum = Checksum.get_checksum_of!(filepath)
    arr = filepath.split(File::SEPARATOR)
    new_path = File.join(arr.slice(0, arr.length-1), checksum)
    FileUtils.cp(filepath, new_path)
    checksum
  end

  def self.get_checksum_file_path!(filepath)
    checksum = Checksum.get_checksum_of!(filepath)
    arr = filepath.split(File::SEPARATOR)
    new_path = File.join(arr.slice(0, arr.length-1), checksum)
    unless File.exist?(new_path)
      FileUtils.cp(filepath, new_path)
    end
    new_path
  end

end
