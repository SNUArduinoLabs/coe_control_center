class Content < ActiveRecord::Base

  before_destroy :check_content_usage

  enum status: [ :not_started, :started, :finished, :error ]
  
	has_many :content_schedules
	has_many :schedules, :through => :content_schedules
	mount_uploader :file, ContentUploader

  def get_file_path
    self.file.file.file.to_s
  end

  def to_jq_upload
    {
      "name" => read_attribute(:file),
      "size" => file.size,
      "url" => file.url,
      "delete_url" => "/contents/#{id}",
      "delete_type" => "DELETE"
    }
  end


  def image?
    if self.content_type
      self.content_type.start_with? 'image'
    else
      false
    end
  end

  def video?
    if self.content_type
      self.content_type.start_with? 'video'
    else
      false
    end
  end

  def pdf?
    if self.content_type
      self.content_type.starts_with? 'application/pdf'
    else
      false
    end
  end

  protected

  def check_content_usage
    return true if self.schedules.empty?

    # TODO
    # self.schedules.each do |s|
    #   return false if s[:end_time] < Time.now
    # end
  end

end
