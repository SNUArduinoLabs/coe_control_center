class Client < ActiveRecord::Base

  belongs_to :user
  has_and_belongs_to_many :groups

  has_many :deployments
  has_many :schedules, :through => :deployments

  has_many :pushes
  has_many :marquees, :through => :pushes

  validates_presence_of :name
  validates_uniqueness_of :name
  validates_presence_of :ip_address
  validates_uniqueness_of :ip_address

  validate do |client|
    errors.add(:base, 'Use correct IP Address format xxx.xxx.xxx.xxx') unless client.valid_v4?
  end

  def valid_v4?
      if /\A(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\Z/ =~ self.ip_address
            return $~.captures.all? {|i| i.to_i < 256}
              end
        return false
  end

end
