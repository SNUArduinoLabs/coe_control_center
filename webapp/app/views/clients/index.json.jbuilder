json.array!(@clients) do |client|
  json.extract! client, :id, :ip_address, :college, :department, :building, :floor, :display_mode, :display_resolution, :display_model
  json.url client_url(client, format: :json)
end
