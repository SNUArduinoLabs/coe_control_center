json.array!(@marquees) do |marquee|
  json.extract! marquee, :id
  json.url marquee_url(marquee, format: :json)
end
