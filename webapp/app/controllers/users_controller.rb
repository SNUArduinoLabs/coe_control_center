class UsersController < ApplicationController

  before_filter :authenticate_admin
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if current_user.is_admin? && @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end

  def update
    respond_to do |format|
      if current_user.is_admin? && @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully udpated' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    respond_to do |format|
      if current_user.is_admin? && !@user.is_admin? && @user.destroy
        format.html { redirect_to users_url, notice: 'User was successfully deleted.' }
      else
        format.html { redirect_to users_url, notice: 'An error occurred while deleting the users.' }
      end
    end
  end


  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:first_name,
                                 :last_name,
                                 :email,
                                 :password,
                                 :password_confirmation)
  end

end
