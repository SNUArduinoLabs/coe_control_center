class ContentSchedulesController < ApplicationController

  def new
      @schedule = Schedule.find_by_id(params[:schedule_id])
      @content_schedule = @schedule.content_schedules.build
  end

  def edit
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @content_schedule = ContentSchedule.find_by_id(params[:id])
  end

  def update
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @content_schedule = ContentSchedule.find_by_id(params[:id])
    content = @content_schedule.content if @content_schedule

    opts = {}
    new_interval = params[:content_schedule][:slide_interval_time]
    old_interval = @content_schedule.slide_interval_time.to_s if @content_schedule
    if old_interval != new_interval
      opts = {
        :is_converted => false
      }
    end


    respond_to do |format|
      if @content_schedule.update(content_schedule_params.merge(opts))

        if content.image?
          ImageConverterJob.perform_later(@content_schedule, content.get_file_path, @content_schedule.get_converted_file_path(new_interval), new_interval, {})
        else
          # VIDEO doesn't need conversion
          @content_schedule.set_converted_filepath!(content.get_file_path)
        end


        format.html { redirect_to edit_schedule_url(@schedule), notice: 'Content was successfully updated.' }
        format.json { render :show, status: :ok, location: @client }
      else
        format.html { render :edit }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @content_schedule = @schedule.content_schedules.build(content_schedule_params)
    content = Content.find_by_id(params[:content_schedule][:content_id])
    interval = params[:content_schedule][:slide_interval_time]
    play_order = params[:content_schedule][:play_order]

    if play_order == "0"
      @content_schedule.play_order = @schedule.get_max_play_order + 1
    end

    respond_to do |format|
      if @content_schedule.save
        if content.image?
          ImageConverterJob.perform_later(@content_schedule, content.get_file_path, @content_schedule.get_converted_file_path(interval), interval, {})
        else
          # VIDEO doesn't need conversion
          @content_schedule.set_converted_filepath!(content.get_file_path)
        end
        format.html { redirect_to @schedule, notice: 'Content was successfully added.' }
        format.json { render :show, status: :created, location: @client }
      else
        format.html { render :new }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @content_schedule = ContentSchedule.find_by_id(params[:id])
    @content_schedule.destroy
    @schedule.rearrange_play_order!
    respond_to do |format|
      format.html {redirect_to edit_schedule_url(@schedule), notice: 'Content removed from the schedule'}
    end

  end


  def up_play_order
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @content_schedule = ContentSchedule.find_by_id(params[:id])
    @all_files = @schedule.get_all_contents_and_attachments
    @max_value = @schedule.get_max_play_order

    msg = ""

    if @content_schedule.play_order == 1
      msg = "Play order not changed"
    else
      before = @all_files.find { |obj| obj.play_order == (@content_schedule.play_order - 1)}
      before.update_attributes(:play_order => @content_schedule.play_order)
      @content_schedule.update_attributes(:play_order => @content_schedule.play_order - 1)
      before.save!
      @content_schedule.save!

      msg = "Play order updated"
    end
    redirect_to edit_schedule_url(@schedule), notice: msg

  end

  def down_play_order
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @content_schedule = ContentSchedule.find_by_id(params[:id])
    @all_files = @schedule.get_all_contents_and_attachments
    @max_value = @schedule.get_max_play_order

    msg = ""

    if @content_schedule.play_order == @max_value
      msg = "Play order not changed"
    else
      after = @all_files.find { |obj| obj.play_order == (@content_schedule.play_order + 1)}
      after.update_attributes(:play_order => @content_schedule.play_order)
      @content_schedule.update_attributes(:play_order => @content_schedule.play_order + 1)
      after.save!
      @content_schedule.save!

      msg = "Play order updated"
    end
    redirect_to edit_schedule_url(@schedule), notice: msg

  end


  private
    def content_schedule_params
      params.require(:content_schedule).permit(:slide_interval_time,
                                              :play_order,
                                              :content_id)
    end

end