class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


  helper_method :current_user
  helper_method :authenticate

  private

  def authenticate
    if current_user.nil?
      redirect_to login_path, :notice => "Please login first to access the dashboard"
    end
  end

  def authenticate_admin
    if current_user.nil? or !current_user.is_admin?
      redirect_to root_path, :notice => "Only admin is authorized for the requested action"
    end
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end


end
