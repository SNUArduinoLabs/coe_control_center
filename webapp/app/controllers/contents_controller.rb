class ContentsController < ApplicationController
  
  before_filter :authenticate

  def index
    @contents = Content.all
  end

  def show
    @content = Content.find(params[:id])
  end

  def edit
    @content = Content.find(params[:id])
  end

  def update
  end

  def new
    @content = Content.new
  end

  def create
    @content = Content.create(content_params)
  end

  def destroy
    @content = Content.find(params[:id])
    respond_to do |format|
      if @content.destroy
        format.html { redirect_to contents_url, notice: 'Content was successfully destroyed.' }
        format.json { head :no_content }
      else
        flash[:alert] = "Content not deleted. Being used by a schedule"
        format.html { render :show }
        format.json { render json: @content.errors, status: :unprocessable_entity }
      end
    end
  end

  private 

  def content_params
    params.require(:content).permit(:file,
                                    :description,
                                    :category)
  end


end
