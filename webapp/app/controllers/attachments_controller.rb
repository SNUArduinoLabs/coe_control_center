class AttachmentsController < ApplicationController

  def new
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @attachment = @schedule.attachments.build
  end

  def edit
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @attachment = Attachment.find_by_id(params[:id])
  end

  def update
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @attachment = Attachment.find_by_id(params[:id])

    opts = {}
    new_interval = params[:attachment][:slide_interval_time]
    old_interval = @attachment.slide_interval_time.to_s if @attachment
    if old_interval != new_interval
      opts = {
        :is_converted => false
      }
    end

    respond_to do |format|
      if @attachment.update(attachment_params.merge(opts))
        DocumentConverterJob.perform_later(@attachment, new_interval, {})
        format.html { redirect_to edit_schedule_url(@schedule), notice: 'Document was successfully updated.' }
        format.json { render :show, status: :ok, location: @client }
      else
        format.html { render :edit }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @attachment = @schedule.attachments.build(attachment_params)
    document = Document.find_by_id(params[:attachment][:document_id])
    interval = params[:attachment][:slide_interval_time]
    play_order = params[:attachment][:play_order]

    if play_order == "0"
      @attachment.play_order = @schedule.get_max_play_order + 1
    end

    respond_to do |format|
      if @attachment.save
        DocumentConverterJob.perform_later(@attachment, interval, {})
        format.html { redirect_to @schedule, notice: 'Document was successfully added.' }
        format.json { render :show, status: :created, location: @client }
      else
        format.html { render :new }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  def up_play_order
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @attachment = Attachment.find_by_id(params[:id])
    @all_files = @schedule.get_all_contents_and_attachments
    @max_value = @schedule.get_max_play_order

    msg = ""

    if @attachment.play_order == 1
      msg = "Play order not changed"
    else
      before = @all_files.find { |obj| obj.play_order == (@attachment.play_order - 1)}
      before.update_attributes(:play_order => @attachment.play_order)
      @attachment.update_attributes(:play_order => @attachment.play_order - 1)
      before.save!
      @attachment.save!

      msg = "Play order updated"
    end
    redirect_to edit_schedule_url(@schedule), notice: msg

  end

  def down_play_order
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @attachment = Attachment.find_by_id(params[:id])
    @all_files = @schedule.get_all_contents_and_attachments
    @max_value = @schedule.get_max_play_order

    msg = ""

    if @attachment.play_order == @max_value
      msg = "Play order not changed"
    else
      after = @all_files.find { |obj| obj.play_order == (@attachment.play_order + 1)}
      after.update_attributes(:play_order => @attachment.play_order)
      @attachment.update_attributes(:play_order => @attachment.play_order + 1)
      after.save!
      @attachment.save!

      msg = "Play order updated"
    end
    redirect_to edit_schedule_url(@schedule), notice: msg

  end

  def destroy
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @attachment = Attachment.find_by_id(params[:id])
    @attachment.destroy
    @schedule.rearrange_play_order!
    respond_to do |format|
      format.html {redirect_to edit_schedule_url(@schedule), notice: 'Document removed from the schedule'}
    end

  end

  private
    def attachment_params
      params.require(:attachment).permit(:slide_interval_time,
                                         :play_order,
                                         :document_id)
    end


end