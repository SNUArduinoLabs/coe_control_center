class SessionsController < ApplicationController
  def new
    if current_user
      redirect_to :dashboard_index
    end
  end


  def create
    user = User.authenticate(params[:email], params[:password])
    if user
      session[:user_id] = user.id
      puts "Login successful"
      redirect_to root_url, :notice => "Logged in"
    else
      flash[:alert] = "Login unseccessful, please try again"
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, :notice => "Logged out"
  end


end
