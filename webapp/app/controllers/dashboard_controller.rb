class DashboardController < ApplicationController

  before_filter :authenticate

  def clients
    @clients = Client.all
  end

  def schedules
    @all_clients = Client.all
    @client = Client.find_by_id(params[:client_id]) || Client.first
    if @client.nil?
        @schedules = []
        @notices = []
    else
        @schedules = @client.schedules
        @notices = @client.marquees
    end
  end

end
