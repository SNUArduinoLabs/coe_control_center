class SchedulesController < ApplicationController

  before_filter :authenticate
  
  MAX_SCHEDULE_PER_PAGE = 50

  def index
  	#@schedules = Schedule.all.limit(10)
  	#@schedules = Schedule.find(:all, :order => "created_at DESC", :limit => MAX_SCHEDULE_PER_PAGE)
    @schedules = Schedule.order("created_at DESC").all()
  end

  def new
  	@schedule = Schedule.new
    @schedule.start_hour = "08"
    @schedule.end_hour = "20"


    @schedule.start_date = Date.today
    @schedule.end_date = Date.today
    @schedule.start_hour = Time.now.hour
    @schedule.end_hour = Time.now.hour
    

    @schedule.is_played_on_monday = true
    @schedule.is_played_on_tuesday = true
    @schedule.is_played_on_wednesday = true
    @schedule.is_played_on_thursday = true
    @schedule.is_played_on_friday = true
  end

  def create
    @schedule = Schedule.new(schedule_params)
    
    respond_to do |format|
      if @schedule.save
        format.html { redirect_to edit_schedule_path(@schedule), notice: 'Schedule was successfully created.' }
        format.js { render js: %(window.location.href='#{edit_schedule_path(@schedule)}') }
      else
        format.html { render :new }
      end
    end
  end

  def edit
  	@schedule = Schedule.find_by_id(params[:id])
    @attachments = @schedule.attachments.sort_by(&:play_order) if @schedule
    @documents = @schedule.documents if @schedule

    @everything = @schedule.get_all_contents_and_attachments if @schedule

    @deployments = @schedule.deployments if @schedule
    @clients = @schedule.clients
    @content_schedules = @schedule.content_schedules.sort_by(&:play_order) if @schedule
    @contents = @schedule.contents if @schedule
  end

  def update
  	@schedule = Schedule.find(params[:id])
  	respond_to do |format|
      if @schedule.update(schedule_params)
        @schedule.update_attributes(:delete_flag => false)
        @clients = @schedule.clients
        flag = true
        unless params[:schedule][:documents].nil?
          params[:schedule][:documents].each { |doc, value|
            if value["enabled"]
              attachment = Attachment.find_or_initialize_by(:document_id => doc.to_i,
                                                            :schedule_id => @schedule.id)
              
              if value["slide_interval_time"].nil? or !value["slide_interval_time"].match(/^[0-9]+$/)
                flag = false
              end

              if value["play_order"].nil? or !value["play_order"].match(/^[0-9]+$/)
                flag = false
              end

              attachment.update_attributes({
                  :schedule_id => @schedule.id,
                  :document_id => doc.to_i,
                  :slide_interval_time => value["slide_interval_time"],
                  :play_order => value["play_order"]
              })
            else
              attachment = Attachment.find_by(:document_id => doc.to_i,
                                              :schedule_id => @schedule.id)
              attachment.destroy unless attachment.nil?
            end
          }
        end

        unless params[:schedule][:contents].nil?
          params[:schedule][:contents].each { |content, value|
            if value["enabled"]
              cs = ContentSchedule.find_or_initialize_by(:content_id => content.to_i,
                                                         :schedule_id => @schedule.id)
              
              if value["slide_interval_time"].nil? or !value["slide_interval_time"].match(/^[0-9]+$/)
                flag = false
              end

              if value["play_order"].nil? or !value["play_order"].match(/^[0-9]+$/)
                flag = false
              end

              cs.update_attributes({
                  :schedule_id => @schedule.id,
                  :content_id => content.to_i,
                  :slide_interval_time => value["slide_interval_time"],
                  :play_order => value["play_order"]
              })
            else
              cs = ContentSchedule.find_by(:content_id => content.to_i,
                                           :schedule_id => @schedule.id)
              cs.destroy unless cs.nil?
            end
          }
        end

        if flag
          format.html { redirect_to @schedule, notice: 'Schedule was successfully updated.' }
          format.js { render js: %(window.location.href='#{edit_schedule_path(@schedule)}') }
        else
          format.html { redirect_to edit_schedule_path(@schedule), alert: "Please enter all the required fields (play_order, interval)" }
        end


      else
        format.html { render :edit }
      end
    end
  end

  def show
  	@schedule = Schedule.find_by_id(params[:id])

    @everything = @schedule.get_all_contents_and_attachments if @schedule
    @deployments = @schedule.deployments if @schedule
    @clients = @schedule.clients
    @content_schedules = @schedule.content_schedules.sort_by(&:play_order) if @schedule
    @contents = @schedule.contents if @schedule
    @attachments = @schedule.attachments.sort_by(&:play_order) if @schedule
    @documents = @schedule.documents if @schedule
  end

  def destroy
    @schedule = Schedule.find(params[:id])
    @deployments = @schedule.deployments
    @deployments.each do |deployment| 
      deployment.update_attributes(:retries_count => 0)
      DestroyDeploymentJob.perform_later(deployment.id)
    end

    @schedule.update_attributes(:delete_flag => true)
    respond_to do |format|
      format.html { redirect_to schedules_url, notice: 'Schedule stop was successfully initiated, it will take some time to finish ' }
      format.json { head :no_content }
    end
  end

  def add_documents 

  end

  def deploy
    @schedule = Schedule.find(params[:id])
    if @schedule.is_deployable and @schedule.deploy!
      redirect_to schedule_url(@schedule), notice: "Deployment started"
    else
      redirect_to schedule_url(@schedule), notice: "Deployment not started conversion pending"
    end
  end

  private
      # Never trust parameters from the scary internet, only allow the white list through.
    def schedule_params
      params.require(:schedule).permit(:description,
                                      :name,
                                      :marquee,
                                      :priority,
                                      :start_hour,
                                      :start_minute,
                                      :end_hour,
                                      :end_minute,
                                      :start_date,
                                      :end_date,
                                      :is_played_on_monday,
                                      :is_played_on_tuesday,
                                      :is_played_on_wednesday,
                                      :is_played_on_thursday,
                                      :is_played_on_friday,
                                      :is_played_on_saturday,
                                      :is_played_on_sunday,
                                      {:client_ids => []},
                                      {:group_ids => []},
                                      {:documents => []},
                                      {:contents => []},
                                      {content_schedules_attributes: [:id,
                                                                      :play_order, 
                                                                      :slide_interval_time, 
                                                                      {:content_attributes => [:id, :file, :description]}
                                                                    ]}
)
    end


end
