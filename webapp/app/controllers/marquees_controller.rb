class MarqueesController < ApplicationController
  before_action :set_marquee, only: [:show, :edit, :update, :destroy, :push]

  # GET /marquees
  # GET /marquees.json
  def index
    @marquees = Marquee.all
  end

  # GET /marquees/1
  # GET /marquees/1.json
  def show
    @clients = @marquee.clients
    @pushes = @marquee.pushes
  end

  # GET /marquees/new
  def new
    @marquee = Marquee.new
    @marquee.start_date = Date.today
    @marquee.end_date = Date.today
    @marquee.start_hour = Time.now.hour
    @marquee.end_hour = Time.now.hour

    @marquee.clients = []
    @clients = []
  end

  # GET /marquees/1/edit
  def edit
    @clients = @marquee.clients
    @pushes = @marquee.pushes
  end

  def push
    @marquee.push!
    @marquee.update_attributes(:is_synced => true)
    redirect_to @marquee, notice: "Push process started"
  end

  # POST /marquees
  # POST /marquees.json
  def create
    @marquee = Marquee.new(marquee_params)
    @clients = Client.all.select {|c| marquee_params[:client_ids].include? c }

    respond_to do |format|
      if @marquee.save
        format.html { redirect_to @marquee, notice: 'Marquee was successfully created.' }
        format.json { render :show, status: :created, location: @marquee }
      else
        format.html { render :new }
        format.json { render json: @marquee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /marquees/1
  # PATCH/PUT /marquees/1.json
  def update
    respond_to do |format|
      if @marquee.update(marquee_params) && @marquee.update_attributes(:is_synced => false)
        format.html { redirect_to @marquee, notice: 'Marquee was successfully updated.' }
        format.json { render :show, status: :ok, location: @marquee }
      else
        format.html { render :edit }
        format.json { render json: @marquee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /marquees/1
  # DELETE /marquees/1.json
  def destroy
  
    @pushes = @marquee.pushes
    @pushes.each do |push|
      push.update_attributes(:retries_count => 0)
      DestroyPushJob.perform_later(push.id)
    end

    respond_to do |format|
      format.html { redirect_to marquees_url, notice: 'Marquee Stop has been iniated successfully, it will take some time to complete.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_marquee
      @marquee = Marquee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def marquee_params
      params.require(:marquee).permit(:content,
                                      :priority,
                                      :start_hour,
                                      :start_minute,
                                      :end_hour,
                                      :end_minute,
                                      :start_date,
                                      :end_date,
                                      {:client_ids => []},
                                      {:group_ids => []})

    end
end
