class DocumentsController < ApplicationController

  def index
    @documents = Document.all
  end

  def new
    @document = Document.new
  end

  def create
    if !params[:images] || params[:images].empty?
      flash[:alert] = "Attachments can't be empty"
      @document = Document.new
      render :new
    else
      @pdf = Document.new(document_params)
      if @pdf.save
        if params[:images]
          params[:images].each { |image|
            @pdf.images.create(file:image)
          }
        end
        flash[:notice] = "Your pdf has been created."
        redirect_to @pdf
      else 
        flash[:alert] = "Something went wrong."
        render :new
      end
    end

  end

  def show
    @document = Document.find_by_id(params[:id])

  end


private
  def document_params
    params.require(:document).permit(:description,
                                  {image_ids: []},
                                  :filename
                                  )
  end



end
