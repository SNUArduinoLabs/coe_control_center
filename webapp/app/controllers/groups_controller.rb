class GroupsController < ApplicationController
  
  before_filter :authenticate
  before_action :set_group, only: [:update, :destroy]

  def index
    @groups = Group.all
  end

  def show
    @group = Group.find_by_id(params[:id])
  end

  # GET /groups/new
  def new
    @group = Group.new
  end


  def edit
    @group = Group.find_by_id(params[:id])
  end

  def create
    @group = Group.new(group_params)
    @group.user = current_user

    respond_to do |format|
      if @group.save
        format.html { redirect_to @group, notice: 'Group was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @group.update(group_params)
        format.html { redirect_to @group, notice: 'Group was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @group.destroy
    respond_to do |format|
      format.html { redirect_to groups_url, notice: 'Group was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_group
      @group = Group.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def group_params
      params.require(:group).permit(:name,
                                    {client_ids: []},
                                    :description,
                                    :building,
                                    :college,
                                    :department,
                                    :group_size,
                                    :contact_info)
    end

end
