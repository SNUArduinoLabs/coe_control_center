class ContentSchedulesController < ApplicationController

  def new
      @schedule = Schedule.find_by_id(params[:schedule_id])
      @content_schedule = @schedule.content_schedules.build
  end

  def edit
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @content_schedule = ContentSchedule.find_by_id(params[:id])
  end

  def create
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @content_schedule = @schedule.content_schedules.build(content_schedule_params)
    content = Content.find_by_id(params[:content_schedule][:content_id])
    interval = params[:content_schedule][:slide_interval_time]
    play_order = params[:content_schedule][:play_order]

    if play_order == "-1"
      @content_schedule.play_order = @schedule.get_max_play_order + 1
    end

    respond_to do |format|
      if @content_schedule.save
        ImageConverterJob.perform_later(@content_schedule, content.get_converted_file_path(interval), interval, {})
        format.html { redirect_to @schedule, notice: 'Content was successfully added.' }
        format.json { render :show, status: :created, location: @client }
      else
        format.html { render :new }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @schedule = Schedule.find_by_id(params[:schedule_id])
    @content_schedule = ContentSchedule.find_by_id(params[:id])
    @content_schedule.destroy
    @schedule.rearrange_play_order!
    respond_to do |format|
      format.html {redirect_to edit_schedule_url(@schedule), notice: 'Content removed from the schedule'}
    end

  end

  private
    def content_schedule_params
      params.require(:content_schedule).permit(:slide_interval_time,
                                              :play_order,
                                              :content_id)
    end

end