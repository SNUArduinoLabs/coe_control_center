class UploaderWorker < ::CarrierWave::Workers::ProcessAsset
  
  # TODO: Have to do error handling, and update other columns of content too

  def success(job)
    p job
    content = Content.find_by_id(job.handler.split("\n")[2].gsub("id: '","").to_i)
    if content
      content[:is_converted] = true
      content.save
    end
  end
end