# encoding: utf-8

require 'carrierwave/processing/mime_types'

class ContentUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  #include CarrierWave::Video  # for your video processing
  #include CarrierWave::FFmpeg
  include CarrierWave::Video::Thumbnailer
  include CarrierWave::FFMPEG


  # For sidekq integration (carrierwave_backgrounder)
  #include ::CarrierWave::Backgrounder::Delay
  include CarrierWave::MimeTypes

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  def default_url
    # For Rails 3.1+ asset pipeline compatibility:
    ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  
    # "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  process :set_content_type
  process :save_content_type_and_size_in_model

  def save_content_type_and_size_in_model
    model.content_type = file.content_type if file.content_type
    model.file_size = file.size
  end


  # Create different versions of your uploaded files:
  version :thumb, :if => :image? do
    process :resize_to_fit => [150, 150]
    process :convert => :png
    #process :set_conversion_finished
    def full_filename(for_file = model.file.file)
      file_name = for_file.to_s.split(".")[0]
      result = [["THUMB_",file_name].join("_"), 'png'].join('.')
      result
    end
  end

  # version :video, :if => :image? do
  #   process :to_video => [:mp4]
  # end

  # # image to video
  # def to_video(format)
  #   options = {}
  #   #self.model.status = 1 # set status as started but not save yet
  #   # add ActiveJob job
  #   self.model.save!
  #   ConverterJob.perform_later(self.model, store_path, options)
  # end

  version :vthumb, :if => :video? do
    process thumbnail: [{format: 'png', quality: 10, size: 192, strip: true, logger: Rails.logger}]
    process :convert => :png
    process :set_conversion_finished
    def full_filename(for_file = model.file.file)
      file_name = for_file.to_s.split(".")[0]
      result = [["THUMB_",file_name].join("_"), 'png'].join('.')
      result
    end
  end


  version :slides, :if => :pdf? do
    process :generate_png
  end

  protected
  def generate_png
    manipulate!(format: "png", density: 400) do |image, index|
      image.format = 'png'
      image.write("#{Rails.root}/public/#{store_dir}/image-#{index}.png")
    end
  end


  # TODO: Set callbacks for failures and progress report
  # for Video
  # TODO: Problematic
  # version :mp4, :if => :video? do
  #   process :encode_video => [:mp4]

  #   def full_filename(for_file = model.file.file)
  #     file_name = for_file.to_s.split(".")[0]
  #     result = [["MP4_",file_name].join("_"), 'mp4'].join('.')
  #     result
  #   end
  # end

  def set_conversion_finished
    self.model.is_converted = true
    self.model.save!
    self.model.finished!
  end

  def encode
    encode_video(:mp4)
    instance_variable_set(:@content_type, "video/mp4")
    :set_content_type_mp4
  end


  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg gif png wmv mp4 avi mov)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

  protected
  def image?(new_file)
    new_file.content_type.start_with? 'image'
  end

  def video?(new_file)
    new_file.content_type.start_with? 'video'
  end

  def pdf?(new_file)
    new_file.content_type.start_with? 'application/pdf'
  end

end
