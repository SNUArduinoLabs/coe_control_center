
namespace :coe_control_center do
  desc "Checks the status of all clients, and persist in Redis"
  task :check_clients_status => :environment do
    puts "There are total of #{Client.all.count} clients."

    clients = Client.all
    clients.each do |client|
      ClientStatusJob.perform_later(client[:id])
    end
  end
end




