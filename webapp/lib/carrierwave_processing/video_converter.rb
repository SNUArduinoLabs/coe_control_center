
require 'streamio-ffmpeg'

module CarrierWave
  module FFMPEG
    module ClassMethods
      def encode_video(target_format)
        process encode_video: [target_format]
      end
    end
 
    def encode_video(format)
      directory = File.dirname(current_path)
      tmpfile = File.join(directory, 'tmpfile')
      File.rename(current_path, tmpfile)
 
      new_name = File.basename(current_path, '.*') + '.' + format.to_s
      current_extenstion = File.extname(current_path).gsub('.', '')

      # TODO
      encoded_file = File.join(directory, new_name)
      # adding some options
      options = { video_min_bitrate: 600, resolution: '320x240', cutome: '-strict -2' }
 
      self.model.status = 1 # set status as started but not save yet
      # add ActiveJob job
      self.model.save!
      ConverterJob.perform_later(tmpfile, encoded_file, self.model, store_path, options)
 
    end
  end
end
