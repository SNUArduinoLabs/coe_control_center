#!/bin/bash -i

# Ubunut 14.04 x64
# make a new user with sudo priviliges
# log in to the user
# and run this file

sudo apt-get -y update
sudo apt-get -y install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev

cd
git clone git://github.com/sstephenson/rbenv.git .rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc

. ~/.bashrc

#$(source ~/.bashrc)
#exec bash

git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
. ~/.bashrc
#$(source ~/.bashrc)
#exec bash

git clone https://github.com/sstephenson/rbenv-gem-rehash.git ~/.rbenv/plugins/rbenv-gem-rehash
. ~/.bashrc
#$(source ~/.bashrc)
#exec bash

rbenv install 2.2.0
rbenv global 2.2.0
rbenv shell 2.2.0
echo "Ruby Version"
ruby -v
. ~/.bashrc

#$(source ~/.bashrc)
#exec bash

# ./install_ffmpeg.sh
sudo add-apt-repository -y ppa:mc3man/trusty-media
sudo apt-get -y update
sudo apt-get -y dist-upgrade
sudo apt-get -y -f install
sudo apt-get -y install libfdk-aac-dev libx264-dev ffmpeg
sudo apt-get -y install ffmpegthumbnailer

. ~/.bashrc

echo "gem: --no-ri --no-rdoc" > ~/.gemrc
gem install bundler

rbenv rehash

sudo add-apt-repository -y ppa:chris-lea/node.js
sudo apt-get -y update
sudo apt-get -y install nodejs

sudo apt-get install -y redis-server
sudo apt-get install -y imagemagick libmagickcore-dev libmagickwand-dev

. ~/.bashrc

cd
git clone https://github.com/SNUArduinoLabs/coe_control_center.git
cd coe_control_center/webapp
gem install bundler foreman
rbenv rehash
bundle install
rbenv rehash
rake db:migrate
rake db:seed


